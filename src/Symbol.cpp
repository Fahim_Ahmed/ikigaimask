#include <ofxSvg.h>

class Symbol {
public:

	ofxSVG symbol;
	string name;

	bool bFlip = false;
	bool bDefaultColor = true;

	int length;

	//prototying
	void processBase(ofxSVG &svg);

	//prototyping end;

	Symbol() {};

	Symbol(ofxSVG p, string name) {
		this->symbol = p;
		this->name = name;

		this->length = symbol.getNumPath();

		processBase(symbol);
	}

	void draw() {		
		for (int i = 0; i < length; i++) {
			symbol.getPathAt(i).draw();
		}
	}

	void processBase(ofxSVG &svg) {
		//int c[] = { 0x6BA591, 0xDFB373 };
		int c[] = { 0x334D5C, 0x45B29D, 0xEFC94C, 0xE27A3F, 0xDF5A49 };
		int cLen = 5;

		//int r = (ofRandom(2) >= 1) ? 1 : 0;

		int len = svg.getNumPath();
		for (int i = 0; i < len; i++) {
			ofPath &p = svg.getPathAt(i);
			p.setPolyWindingMode(OF_POLY_WINDING_ODD);

			if (bDefaultColor) {
				p.getUseShapeColor();
				//p.setFillHexColor(0x2E3191);
			}
			else {
				int r = int(ofRandom(0, 5));
				p.setFillColor(ofColor::fromHex(c[r]));
			}
		}
	}
};