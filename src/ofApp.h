#pragma once

#include "ofMain.h"
#include "ofxSvg.h"

class ofApp : public ofBaseApp{
private:
	ofxSVG symbolSVG;
	ofxSVG baseSVG;

	ofVec2f **points;
	ofFbo fbo;
	vector<ofPath> symbols;
	vector<ofColor> colours;

	int sw, sh;
	bool bShowCoords;
	bool bDefaultColor;

	public:
		void setup();
	void generateMask();
	void processSymbols(ofxSVG& svg);
	void processBase(ofxSVG& svg);
	ofVec2f coordToPoint(int i, int j);

	void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
