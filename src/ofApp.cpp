#include "ofApp.h"

//#define BLACK_BACKGROUND



float w = 752;
float h = 752;

float cellWidth = 94;
float cellHeight = 94;

float ox = cellWidth * 0.5f;
float oy = cellHeight * 0.5f;

int xPoints = w / cellWidth;
int yPoints = h / cellHeight;

int halfXP = xPoints * 0.5f;


//--------------------------------------------------------------
void ofApp::setup(){
	ofSetFrameRate(120);
	ofBackground(255);

#ifdef BLACK_BACKGROUND
	ofBackground(0);
#endif

	ofSetVerticalSync(false);
	ofEnableAntiAliasing();

	sw = ofGetWidth();
	sh = ofGetHeight();

	bShowCoords = false;
	bDefaultColor = true;

	symbolSVG.load("symbols_94x94.svg");
	baseSVG.load("base_282x188.svg");

	points = new ofVec2f*[yPoints * 2];
	for (int i = 0; i < yPoints * 2; i++)
		points[i] = new ofVec2f[xPoints];

	processSymbols(symbolSVG);
	processBase(baseSVG);
	generateMask();
}

void ofApp::generateMask() {
	fbo.allocate(w * 2, h * 2, 6408, 32);
	fbo.begin();

	ofPushMatrix();
	ofTranslate(w * 0.5f, h * 0.5f);

	for (int i = -yPoints; i < yPoints; i++) {
		for (int j = -halfXP; j < halfXP; j++) {

			float px = j * cellWidth + ((abs(i % 2) == 1) ? ox : 0);
			float py = i * oy;

			points[i + yPoints][j + halfXP].x = px;
			points[i + yPoints][j + halfXP].y = py;

			ofSetColor(0);
			ofDrawRectangle(px - 1, py - 1, 2, 2);
			
			string s = ofToString(i) + "," + ofToString(j);
			if(bShowCoords) ofDrawBitmapString(s, px, py);
		}
	}

	//draw base
	ofPushMatrix();
	ofTranslate(coordToPoint(0, 0));
	baseSVG.draw();
	ofPopMatrix();

	//draw eye
	int count = symbols.size();
	int r = int(ofRandom(0, count));

	ofPath eyeL = symbols.at(r);
	ofPath eyeR = symbols.at(r);

	ofVec2f coord = coordToPoint(0, 1);
	coord -= ofVec2f(cellWidth * 0.5f, 0);
	eyeL.rotate(r * 90, ofVec3f(0, 0, 1));
	eyeL.draw(coord.x, coord.y);

	coord = coordToPoint(0, -1);
	coord += ofVec2f(cellWidth * 0.5f, 0);
	eyeR.rotate(r * 90, ofVec3f(0, 0, 1));
	eyeR.scale(-1, 1);	
	eyeR.draw(coord.x, coord.y);

	//draw mouth
	r = int(ofRandom(0, count));
	ofPath mouth = symbols.at(r);
	coord = coordToPoint(2, 0);
	if (ofRandom(0,2) >= 0.5f) coord -= ofVec2f(0, -cellHeight * 0.5f);
	eyeL.rotate(r * 90, ofVec3f(0, 0, 1));
	mouth.draw(coord.x, coord.y);

	//draw etc
	if (ofRandom(0, 2) >= 1) {
		r = int(ofRandom(0, count));
		ofPath cheekR = symbols.at(r);

		coord = coordToPoint(1, 1);
		coord -= ofVec2f(cellWidth * 0.5f, 0);
		cheekR.rotate(r * 90, ofVec3f(0, 0, 1));
		cheekR.draw(coord.x, coord.y);

		ofPath cheekL = symbols.at(r);
		coord = coordToPoint(1, -1);
		coord -= ofVec2f(cellWidth * 0.5f, 0);
		cheekL.rotate(r * 90, ofVec3f(0, 0, 1));
		cheekL.scale(-1, 1);
		cheekL.draw(coord.x, coord.y);
	}

	if (ofRandom(0, 2) >= 1) {
		r = int(ofRandom(0, count));
		ofPath cheekR = symbols.at(r);

		coord = coordToPoint(1, 1);
		coord -= ofVec2f(cellWidth * 0.5f, -cellHeight * 0.5f);
		cheekR.rotate(r * 90, ofVec3f(0, 0, 1));
		cheekR.draw(coord.x, coord.y);

		ofPath cheekL = symbols.at(r);
		coord = coordToPoint(1, -1);
		coord -= ofVec2f(cellWidth * 0.5f, -cellHeight * 0.5f);
		cheekL.rotate(r * 90, ofVec3f(0, 0, 1));
		cheekL.scale(-1, 1);
		cheekL.draw(coord.x, coord.y);
	}

//	ofNoFill();
//	ofSetColor(0);
//	ofDrawCircle(coordToPoint(0, 0), 8);
//	ofFill();

	ofPopMatrix();

	fbo.end();
}


//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
	ofPushMatrix();
//	ofTranslate(320 + 40, 320 + 40);
	
	ofSetColor(255);
	fbo.draw(24, 24);	

	ofPopMatrix();
}

void ofApp::processSymbols(ofxSVG &svg) {
	int len = svg.getNumPath();
	
	//int c[] = { 0x272321,0x37312c,0x605f55,0x711f2f,0xe87e85,0x18715f };
	int c[] = { 0x334D5C, 0x45B29D, 0xEFC94C, 0xE27A3F, 0xDF5A49 };
	int cLen = 5;
	for (int i = 0; i < cLen; i++) {
		colours.push_back(ofColor::fromHex(c[i]));
	}

	symbols.clear();

	for (int i = 0; i < len; i++) {
		ofPath p = svg.getPathAt(i);
		p.setPolyWindingMode(OF_POLY_WINDING_ODD);

		if (bDefaultColor) {
			p.setUseShapeColor(true);
			p.setFillHexColor(0x2E3191);
			p.setStrokeWidth(1);
			p.setStrokeHexColor(0x2E3191);
		}else {
			int r = int(ofRandom(0, cLen));
			
			p.setUseShapeColor(false);
			p.setFillColor(colours.at(r));
			p.setStrokeColor(colours.at(r));
			p.setStrokeWidth(1);
			
		}

		symbols.push_back(p);
	}
}

void ofApp::processBase(ofxSVG &svg) {
	//int c[] = { 0x6BA591, 0xDFB373 };
	int c[] = { 0x334D5C, 0x45B29D, 0xEFC94C, 0xE27A3F, 0xDF5A49 };
	int cLen = 5;
	
	//int r = (ofRandom(2) >= 1) ? 1 : 0;
	
	int len = svg.getNumPath();
	for (int i = 0; i < len; i++) {
		ofPath &p = svg.getPathAt(i);
		p.setPolyWindingMode(OF_POLY_WINDING_ODD);

		if(bDefaultColor) {
			p.getUseShapeColor();	
			//p.setFillHexColor(0x2E3191);
		}
		else {
			int r = int(ofRandom(0,5));
			p.setFillColor(ofColor::fromHex(c[r]));
//			p.setStrokeColor(ofColor::fromHex(c[r]));
//			p.setStrokeWidth(1);
		}
	}
}

ofVec2f ofApp::coordToPoint(int i, int j) {
	int ox = yPoints;
	int oy = xPoints / 2;

	ofVec2f v = points[i + ox][j + oy];
	//v.x = v.x + cellWidth * 0.5f;
	//v.y -= cellHeight * 0.5f;

	return v;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	switch(key) {
	case 'd':
		bShowCoords = !bShowCoords;
		break;
	case 'c':
		bDefaultColor = !bDefaultColor;
		break;
	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	processSymbols(symbolSVG);
	processBase(baseSVG);
	generateMask();
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	sw = w;
	sh = h;
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
